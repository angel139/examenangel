<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'My Awesome Exam';
?>
<div class="site-index">
    <?= $enunciado1; ?>
    <?= 
        
        GridView::widget([
        'dataProvider' => $dataProvider1 ,
        'columns' => [
           
            
            'nombre',
            'dorsal',
            
        ]
    ]);?>
    <?= $enunciado2; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider2 ,
        'columns' => [
            
            
            'numetapa',
            'kms',
            
        ]
    ]); ?>
    <?= $enunciado3 ; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider3 ,
        'columns' => [
            
            
            'nompuerto',
            'dorsal',
            
        ]
    ]); ?>
    

</div>
