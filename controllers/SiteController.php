<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $consulta1 = new SqlDataProvider([
            'sql' => ('SELECT nombre, dorsal FROM ciclista WHERE edad > 25 AND edad < 35'),
            
        ]);
        $consulta2 = new SqlDataProvider([
            'sql' => ('SELECT numetapa, kms FROM etapa WHERE llegada = salida'),
            
        ]);
         $consulta3 = new SqlDataProvider([
            'sql' => ('SELECT nompuerto,dorsal FROM puerto WHERE altura>1500'),
            
        ]);

        return $this->render('index', [
            'dataProvider1' => $consulta1,
            'enunciado1' => 'Los ciclistas cuya edad está entre 25 y 35 años mostrando únicamente el dorsal y el nombre del ciclista',
            'dataProvider2' => $consulta2,
            'enunciado2' => 'Las etapas circulares mostrando sólo el número de etapa y la longitud de las mismas',
            'dataProvider3' => $consulta3,
            'enunciado3' => 'Los puertos con altura mayor a 1500 metros figurando sólo el nombre del puerto y el dorsal del ciclista que lo ganó'
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta2(){
       $dataProvider = new SqlDataProvider([
            'sql' => ('SELECT numetapa, kms FROM etapa WHERE llegada = salida'),
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'enunciado' => 'Las etapas circulares mostrando sólo el número de etapa y la longitud de las mismas'
        ]);
        
    }
    public function actionConsulta3(){
       $dataProvider = new ActiveDataProvider([
            'sql' => ('SELECT nompuerto,dorsal FROM puerto WHERE altura>1500'),
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'enunciado' => 'Los puertos con altura mayor a 1500 metros figurando sólo el nombre del puerto y el dorsal del ciclista que lo ganó'
        ]);
        
    }
}
